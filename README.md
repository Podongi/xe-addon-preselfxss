# Self-XSS 예방 애드온
Console에 문구를 넣어 사용자가 잘 알지 못하는 코드를 붙여 넣는 것을 예방해주는 애드온입니다.

## 기능 소개
* 브라우저에서 제공하는 Console에 console.log를 사용해 경고 문구를 삽입합니다.

## 안내
* 일부 브라우저에서는 경고 문구가 표시되지 않습니다. 이럴 때에는 preselfxss.addon.php의 $presxcs를 수정하세요.
`
if( window.console == undefined ) { console = {log : function(){} }; }
`
